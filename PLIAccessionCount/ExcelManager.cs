﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.IO;
using PLIAccessionCount.Models;

namespace PLIAccessionCount
{
    public class ExcelManager
    {
            //This ExcelManager class achieves two major tasks:
            //1) populate the territory spreadsheet one for each territory
            //2) populate the master count spreadsheet only one sheet for all territories
            //It initiates with a list of territories, for each of which a class object is to include
            //its territory name, its spreadsheet file name, and total territory accession count.

        private Application excelApp;
        private Workbook wbook;
        private Worksheet wsheet;
        private readonly string masterCountFile;
        private readonly string sMonthName;
        private readonly string excelRowDateLabel;
        private readonly string excelColDateLabel;

        private string _dateLabel;
        private List<string> territoryList = new List<string> { "Cleveland", "Columbus", "Dayton", "Ft Wayne",
                                                                "Indianapolis", "Lima", "Zanesville", "Zangmeister"};
        private List<Territory> Territories = new List<Territory>();

        public ExcelManager(string DateLabel)
        {
            _dateLabel = DateLabel;

                    //for updating each territory file
            foreach (string terName in territoryList)
            {
                Territory terObj = new Territory()
                {
                    TerritoryName = terName,
                    TerritoryAccessionCountFileName = terName + " Accession Count Comparison " + _dateLabel.Substring(4, 4) + 
                                            ConfigurationManager.AppSettings["excelType"],
                    TerritoryTotal = 0
                };

                Territories.Add(terObj);
            }

            //for updating the master count file
            masterCountFile = "Clinical Accession Counts " + _dateLabel.Substring(4, 4) + ConfigurationManager.AppSettings["excelType"];
            sMonthName = Program.dateLabels.GetExcelEnglishMonth();                 //this value is used to find spreadsheet by name
            excelRowDateLabel = Program.dateLabels.GetRowDateLabel();               //this value is used to find data entry row
            excelColDateLabel = Program.dateLabels.GetColDateLabel();               //this value is used to find data entry column

            excelApp = new Application();
            excelApp.Visible = false;
        }

        // PROCESS TERRITORY SPREADSHEETS ONE FILE AT A TIME FOR ALL TERRITORIES
        public void UpdateTerritoryData(Dictionary<string, CsvRecord> csvRecords)
        {
            Logger.WriteToLog(" ");
            Logger.WriteToLog("Going through the territories...");

            try
            { 
                foreach (Territory thisTerObj in Territories)
                {
                    Logger.WriteToLog($"\tAccessing {thisTerObj.TerritoryName} ...");

                    string TerritoryCountComparisonFile = Path.Combine(ConfigurationManager.AppSettings["excelFolder"],
                                                                    thisTerObj.TerritoryAccessionCountFileName);
                    Console.WriteLine("Excel: {0}", TerritoryCountComparisonFile);
                    
                    wbook = excelApp.Workbooks.Open(TerritoryCountComparisonFile);

                    //test if worksheet exists for the month
                    if (IfSheetExist(sMonthName))
                    {
                        wsheet = wbook.Sheets[sMonthName];
                    }
                    else         
                    {
                        Console.WriteLine("Worksheet not found. Skipped.");
                        Logger.WriteToLog($"Error: Month sheet not found for {sMonthName}. Territory skipped.");
                        goto NoSheetComeHere;              //sheet not exists, skip the territory
                    }
                    
                    
                    // find the Date row index
                    int rowDate = 1;
                    while (wsheet.Cells[rowDate, 1].Text.ToString().Trim() != "Date")
                    {
                        rowDate++;
                    }

                    // find the row index for the Total row in column 1
                    int rowTotal = rowDate;
                    while (wsheet.Cells[rowTotal, 1].Text.ToString().Trim() != "Total")
                    {
                        rowTotal++;
                    }

                    // find the row index for data insertion by matching date heading in Column 1
                    int rowData = 1;
                    for (int row = 1; row < rowTotal; row++)
                    {
                        if (wsheet.Cells[row, 1].Text.ToString().Trim() == excelRowDateLabel)
                        {
                            rowData = row;
                            break;
                        }
                    }

                    // find Clinical column index to set max column num for data insertion
                    int colClinical = 1;
                    while (wsheet.Cells[rowDate, colClinical].Text.ToString().Trim() != "Clinical")
                    {
                        colClinical++;
                    }

                    // initialize the data entry row to 0 if column is not a Total column (such as in the Lima sheet)
                    for (int col = 2; col < colClinical; col++)
                    {
                        if (wsheet.Cells[rowDate, col].Text.ToString().Trim() != "Total")
                        {
                            wsheet.Cells[rowData, col].Value2 = 0;
                        }
                    }

                    // insert csv values into the data row if input tag matches the column headings
                    for (int col = 2; col < colClinical; col++)
                    {
                        CsvRecord csvrec = csvRecords.Where(t => t.Key == wsheet.Cells[rowDate, col].Value2).FirstOrDefault().Value;
                        if (csvrec != null)
                        {
                            wsheet.Cells[rowData, col].Value2 = csvrec.Count;
                            csvrec.CheckAsUsed(csvrec.Tag);
                        }
                    }

                // get territory row total count
                    thisTerObj.TerritoryTotal = (int)wsheet.Cells[rowData, colClinical].Value2;

                    Logger.WriteToLog($"\tTerritory: {thisTerObj.TerritoryName}, Month: {sMonthName}, Total: {thisTerObj.TerritoryTotal}.");
                    Logger.WriteToLog($"\t\tSaving {thisTerObj.TerritoryName} file...");
                
NoSheetComeHere:
                    wbook.Save();
                    wbook.Close();

                    System.Runtime.InteropServices.Marshal.ReleaseComObject(wbook);

                    Logger.WriteToLog($"\t\tFinished with {thisTerObj.TerritoryName} file. " +
                                                    $"Clinical Total is: {thisTerObj.TerritoryTotal}");
                }

                Logger.WriteToLog("\nMoving on to master count...");
            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.Message);
            }
        }

        // UPDATE THE MASTER COUNT SPREADSHEET WITH TERRITORY TOTALS AND FILLING IN OTHER TAGGED VALUES
        public void SetMasterCounts(Dictionary<string, CsvRecord> csvRecords, int csvCountAccountable)
        {
            int wsheetTotalAccessionCount = 0;
            int dateRowId = 1;                      //the row containing the needed Date in column and starting with "PREFIX"
            int entryColId = 0;                     //the column into which new data are to be inserted if a date is matched
            int totalRowId = 0;                     //the ending row containing Total or Totals for the spreadsheet in column 3
            int remoteStartRowId = 0;               //1st row of the REMOTES section
            int remoteEndRowId = 0;                 //last row of the REMOTES section
            string strValue = string.Empty;     

            string MasterCountExcelFile = Path.Combine(ConfigurationManager.AppSettings["excelFolder"], masterCountFile);
            Logger.WriteToLog($"Processing master count sheet: {masterCountFile} for the month of {sMonthName}...");

            wbook = excelApp.Workbooks.Open(MasterCountExcelFile);
            wsheet = wbook.Sheets[sMonthName];

            // find the row num for the Date heading in column 1
            while (wsheet.Cells[dateRowId, 1].Text.ToString().Trim() != "PREFIX")
            {
                dateRowId++;
            }

            // moving right to find the dated column for the current date, new data will fill this column
            for (int col = 1; col <= 50; col++)
            {
                if (wsheet.Cells[dateRowId, col].Text.ToString().Trim() == excelColDateLabel)
                {
                    entryColId = col;
                    break;
                }
            }

            // find the row num for the Total row in column 3
            int iCounter = 1;
            while (wsheet.Cells[iCounter, 3].Text.ToString().Trim() != "TOTAL" &&
                                                            wsheet.Cells[iCounter, 3].Text.ToString().Trim() != "TOTALS")
            {
                iCounter++;
            }
            totalRowId = iCounter;

            // find the starting row for Remotes data block in column 1
            iCounter = 1;
            while (wsheet.Cells[iCounter, 1].Text.ToString().Trim() != "REMOTES" &&
                    wsheet.Cells[iCounter, 1].Text.ToString().Trim() != "REMOTES (Misys Order Entry)")
            {
                iCounter++;
            }
            remoteStartRowId = iCounter;

            // find the Subtotal row for Remotes data block in column 3
            while (wsheet.Cells[iCounter, 3].Text.ToString().Trim() != "Subtotal REMOTES")
            {
                iCounter++;
            }
            remoteEndRowId = iCounter;

            // compare sheet row heading with csv dataset
            // if matched, put csv counts in its the column found earlier for this date
            for (int row = dateRowId + 2; row <= totalRowId - 2; row++)
            {
                string RowTag = wsheet.Cells[row, 1].Text.ToString().Trim();

                if (!string.IsNullOrEmpty(RowTag) && RowTag.Length <= 2)
                {
                    CsvRecord thisCsvRecord = csvRecords.Where(t => t.Key == RowTag).FirstOrDefault().Value;
                    if (thisCsvRecord != null) 
                    {
                        // update cell with count from csv and flag row tag as used
                        wsheet.Cells[row, entryColId].Value2 = thisCsvRecord.Count;
                        thisCsvRecord.CheckAsUsed(thisCsvRecord.Tag);
                    }
                    else
                    {
                        // if no match count, initialize cell to 0
                        wsheet.Cells[row, entryColId].Value2 = 0;
                    }
                }
            } 

            wbook.Save();

            // This block of code inserts the territory totals into the Remote block on the master count file
            for (int row = remoteStartRowId; row < remoteEndRowId; row++)
            {
                strValue = wsheet.Cells[row, 3].Text.ToString().Trim();
                Territory terObj = new Territory();

                if ( strValue.Contains("Cleveland") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Cleveland").FirstOrDefault();
                }
                else if ( strValue.Contains("Columbus") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Columbus").FirstOrDefault();
                }
                else if ( strValue.Contains("Dayton") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Dayton").FirstOrDefault();
                }
                else if ( strValue.Contains("Lima") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Lima").FirstOrDefault();
                }
                else if ( strValue.Contains("Indianapolis") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Indianapolis").FirstOrDefault();
                }
                else if (strValue.Contains("Indiana (") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Indiana").FirstOrDefault();
                }
                else if ( strValue.Contains("Ft. Wayne") || strValue.Contains("Ft Wayne") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Ft Wayne").FirstOrDefault();
                }
                else if ( strValue.Contains("Zanesville") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Zanesville").FirstOrDefault();
                }
                else if ( strValue.Contains("Zangmeister") )
                {
                    terObj = Territories.Where(t => t.TerritoryName == "Zangmeister").FirstOrDefault();
                }

                if ( terObj != null )
                {
                    wsheet.Cells[row, entryColId].Value2 = terObj.TerritoryTotal;
                }
            }

            wbook.Save();
            wsheetTotalAccessionCount = (int)wsheet.Cells[totalRowId, entryColId].Value2;
            wbook.Close();
            excelApp.Quit();

            System.Runtime.InteropServices.Marshal.ReleaseComObject(wbook);
            //System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelApp);

            // // // // // // ERROR CHECKING // // // // //
            // This section checks for unused tags on the Master Count spreadsheet 
            Logger.WriteToLog($"Master counts set. CSV reportable input count: {csvCountAccountable} vs " +
                                    $"spreadsheet count: {wsheetTotalAccessionCount}.");
            Logger.WriteToLog($"Error checking...");

            int errorCount = 0;
            string unusedTags = string.Empty;

            foreach (KeyValuePair<string, CsvRecord> csvPair in csvRecords)
            {
                CsvRecord csvRec = csvPair.Value;
                if (!csvRec.IsUsed)
                {
                    Logger.WriteToLog($"Tag {csvRec.Tag}: value {csvRec.Count} was never used.");

                    errorCount += csvRec.Count;
                    unusedTags += csvRec.Tag + ",";
                }
            }

            if (errorCount==0)
            {
                Logger.WriteToLog($"All values matched and reported.");
            }

            if (unusedTags.Length > 0)
            {
                unusedTags = unusedTags.Substring(0, unusedTags.Length - 1);              //remove the trailing comma
            }

            // This section checks if the values match and call sending email nofitications
            string emailMessage;
            int emailType;
            if (csvCountAccountable == 0 || wsheetTotalAccessionCount == 0)
            {
                emailType = 0;
                emailMessage = $"No accession data were loaded for {_dateLabel}.\n";
            }
            else if (csvCountAccountable == wsheetTotalAccessionCount)
            {
                emailType = 1;
                emailMessage = $"Accession count totals matched for {_dateLabel}.\n";
            }
            else if (csvCountAccountable == wsheetTotalAccessionCount + errorCount)
            {
                emailType = 4;
                emailMessage = $"Accession count totals did not match for {_dateLabel}.\n" +
                         $"\t\t\tThe following prefixes could not be found on the spreadsheets: {unusedTags}; Count not matched: {errorCount}.\n";
            }
            else if (csvCountAccountable != wsheetTotalAccessionCount + errorCount)
            {
                emailType = 5;
                emailMessage = $"Accession count totals did not match for {_dateLabel}. " +
                            $"The following prefixes could not be found on the spreadsheets: {unusedTags}, resulting in an " +
                            $"accession count error of {errorCount}. " +
                            $"There are errors in addition to the listed missing prefixes. Thus, manual intervention is required.";
            }
            else
            {
                emailType = 2;
                emailMessage = $"Accession count totals did not match for {_dateLabel}.\n";
            }

            // Call to send email notification 
            if (ConfigurationManager.AppSettings["sendEmail"] == "Y")
            {
                Logger.WriteToLog("Sending email...");
                Logger.WriteToLog($"\tEmail Type: {emailType}\tEmail Message: {emailMessage}");
                SendEmail(emailType, emailMessage);
               //Logger.WriteToLog("Job ran successfully. Email notification sent.");
            }
            else 
            {
                
                Logger.WriteToLog("Job ran successfully. But no email notification sent.");
            }
        }

        private void SendEmail(int msgType, string mailMessage)
        {
            string eSubject = "WARNING -- Daily PLI Accession Count";

            if (msgType == 1) eSubject = "Daily PLI Accession Count";

            SmtpClient client = new SmtpClient();
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.Host = ConfigurationManager.AppSettings["clientHost"];
            client.Port = int.Parse(ConfigurationManager.AppSettings["emailPort"]);
            client.DeliveryMethod = SmtpDeliveryMethod.Network; 
            client.UseDefaultCredentials = true;

            MailMessage mail = new MailMessage();
            mail.To.Add(ConfigurationManager.AppSettings["emailTo"]);
            mail.From = new MailAddress(ConfigurationManager.AppSettings["emailFrom"]);
            mail.CC.Add(ConfigurationManager.AppSettings["emailCC"]);
            mail.Subject = eSubject;
            mail.IsBodyHtml = true;
            mail.Body = mailMessage;

            try
            {
                client.Send(mail);
                Logger.WriteToLog("Job ran successfully.");
                Logger.WriteToLog($"Sent email notification using: {client.Host} from {mail.From} at port {client.Port}.");
                Logger.WriteToLog($"Sent a type-{msgType} mail notice. Subject: {eSubject}");
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    Logger.WriteToLog($"Error Inner: {e.InnerException}");
                }
            }
        }

        private bool IfSheetExist(string sheetName)
        {
            foreach ( Worksheet sht in wbook.Sheets)
            {
                if (sht.Name == sheetName)
                {
                    return true;
                }
            }
            
            return false;
        }
    }
}