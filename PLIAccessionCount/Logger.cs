﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace PLIAccessionCount
{
    public static class Logger
    {

        public static void WriteToLog(string message)
        {
            string logfile = Path.Combine(ConfigurationManager.AppSettings["logFolder"],
                                           "accessioncount" + Program.dateLabels.GetCsvFileDateLabel() + ".txt");
            
            using (StreamWriter writer = new StreamWriter(logfile, true))
            {
                writer.WriteLine($"{DateTime.Now} : {message}");
            }
        }
    }
}
