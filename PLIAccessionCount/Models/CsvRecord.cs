﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLIAccessionCount.Models
{
    public class CsvRecord
    {
        public string Tag;
        public int Count = 0;
        public bool IsUsed = false;
        public bool IsReportable = true;

        public void CheckAsUsed()
        {
            //these tags are omitted from report by default
            if ( Tag == "CC" || Tag == "EP" || Tag == "LC" || Tag == "LS" || Tag == "OS" ||
                 Tag == "PC" || Tag == "PL" || Tag == "PS" || Tag == "PT")
            {
                IsUsed = true;
                IsReportable = false;
            }
        }

            //this overloads the above. the incoming tag is marked as used if a match is found on the spreadsheet
        public void CheckAsUsed(String thisTag)
        {
            if (Tag == thisTag)
            {
                IsUsed = true;
            }
        }
    }
}
