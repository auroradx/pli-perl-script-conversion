﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLIAccessionCount.Models
{
    public class Territory
    {
        public String TerritoryName { get; set; }
        
        public string TerritoryAccessionCountFileName { get; set; }
        
        public int TerritoryTotal { get; set; }
    }
}