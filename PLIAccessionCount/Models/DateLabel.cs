﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLIAccessionCount.Models
{
    public class DateLabel
    {
        // This class contains all the dates and date label strings that are used to configure 
        //      file loading and spreadsheet value insertion.
        // The CSV input data file receives a pre-set date from the program and finds the such dated file for input.
        // Excel insertion is one day before the input file date (execution date).   
        // Date labels are used to find the matched CSV file and to find rows and columns for data insertion into spreadsheets.
         
        private static DateTime CsvDateTime; 
        private static DateTime ExcelDateTime;

        public DateLabel(DateTime csvFileDate)
        {
            //input file date is the same as the execution date
            CsvDateTime = csvFileDate;
            //Excel data insertion date is one day bedore the input file date
            ExcelDateTime = CsvDateTime.AddDays(-1);
        }

        public string GetCsvFileDateLabel()
        {
                // return input file string date in mmddyyyy form
            return CsvDateTime.Month.ToString("d2") + CsvDateTime.Day.ToString("d2") +
                                                             CsvDateTime.Year.ToString("d4");
        }

        public string GetExcelValueDateLabel()
        {       // return Excel data insertion date in mmddyyyy form
            return ExcelDateTime.Month.ToString("d2") + ExcelDateTime.Day.ToString("d2") +
                                                             ExcelDateTime.Year.ToString("d4");
        }

                // return the English month of the Excel date, used to find the correct worksheet by name
        public string GetExcelEnglishMonth()
        {
            return ExcelDateTime.ToString("MMMM");
        }

                // return Excel date in mm/dd/yy format to find the row date heading in territory sheet.
        public string GetRowDateLabel()
        {
            return ExcelDateTime.ToString("MM/dd/yy");
        }

                // return Excel date in m/d, m/dd, mm/d, mm/dd format to the column date heading in master count sheet.
        public string GetColDateLabel()
        {
            return ExcelDateTime.Month.ToString() + "/" + ExcelDateTime.Day.ToString();
        }

    }
}
