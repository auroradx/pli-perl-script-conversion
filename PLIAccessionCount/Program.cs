﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using PLIAccessionCount.Models;

namespace PLIAccessionCount
{ 
    class Program
    {
        private static int goBackDays = int.Parse(ConfigurationManager.AppSettings["goBackDays"]);

        public static DateLabel dateLabels = new DateLabel(DateTime.Now.AddDays(-goBackDays));

        static void Main(string[] args) 
        { 
            try
            {
                Console.WriteLine($"Input date: { dateLabels.GetCsvFileDateLabel() }; Excel date:  { dateLabels.GetExcelValueDateLabel()} ");

                // Read CSV FILE AND RETURN DATA SET
                DataManager dataManager = new DataManager(dateLabels.GetCsvFileDateLabel());
                 
                Dictionary <string, CsvRecord> CsvKeyValuePairs = dataManager.GetCsvInputData();

                if (CsvKeyValuePairs.Count <= 0)
                {
                    Logger.WriteToLog("No CSV input values found.");
                }

                // NOW PROCESS EXCEL SPREADSHEETS
                ExcelManager excelManager = new ExcelManager(dateLabels.GetExcelValueDateLabel());

                // write to Territory files
                excelManager.UpdateTerritoryData(CsvKeyValuePairs);

                // write to Master Count file
                excelManager.SetMasterCounts(CsvKeyValuePairs, dataManager.GetCsvTotalCount(CsvKeyValuePairs));

            }
            catch (Exception e)
            {
                Logger.WriteToLog(e.Message);
            }
        }
    }
}
