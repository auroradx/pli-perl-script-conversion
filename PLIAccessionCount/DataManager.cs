﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Configuration;
using PLIAccessionCount.Models;

namespace PLIAccessionCount
{
    public class DataManager
    {
        //This DataManager class takes an input date string, finds the so dated CSV input file, and
        //   reads each row (pair of tag and count) into a dictioncary of tag and CsvRecord objects.
        //It returns the dictionary and also returns the total count of values in all input rows.
        
        private string infile;
        private string _dateLabel;
         
        public DataManager(string DateLabel)
        {
            _dateLabel = DateLabel;
            infile = Path.Combine(ConfigurationManager.AppSettings["csvFolder"], "RAW_ACCN_COUNT_" + DateLabel + ".csv");
        }

        // This method reads the input CSV file and return a dictionary of key and object values, 
        //  with each input line being an entry in the dictionary.
        public Dictionary <string, CsvRecord> GetCsvInputData()
        {
            var CsvRecords = new Dictionary<string, CsvRecord>();

            string CsvInputData = File.ReadAllText(infile);

            Logger.WriteToLog($"CSV file opened: {infile}");
            Logger.WriteToLog($"CSV file date: {_dateLabel}. Gathering information...");

            string Tag;
            foreach(string line in CsvInputData.Split('\n'))
            {
                if (!string.IsNullOrEmpty(line))
                {
                    var csvRecord = new CsvRecord();
                    
                    Tag = line.Split(',')[0].Replace("\"", "");         //also remove double quotes from csv text input
                    
                    //if Tag is in the form of A#, B#, C# ... etc., where # is an integer, only take the first character as tag
                   // if ( CheckIsInt(Tag.Substring(1, 1)) )
                   // {
                   //     Tag = Tag.Substring(0, 1);
                    //}

                    csvRecord.Tag = Tag;           
                    csvRecord.Count = Int32.Parse(line.Split(',')[1]);
                    csvRecord.CheckAsUsed();
                    
                    CsvRecords.Add(csvRecord.Tag, csvRecord);

                    Logger.WriteToLog($"{csvRecord.Tag} : {csvRecord.Count}");
                }
            }

            Logger.WriteToLog($"Success reading CSV input data");

            return CsvRecords;
        }

          // This method sums and returns total count from all csv line values
          // This total number is used to compare with the total from Excel entries for error checking purposes
        public int GetCsvTotalCount(Dictionary<string, CsvRecord> csvRecords)
        {
            int CountReportable = 0;
            int TotalCount = 0;

            foreach (KeyValuePair<string, CsvRecord> keyvalpair in csvRecords)
            {
                CsvRecord csvRec = keyvalpair.Value;

                TotalCount += csvRec.Count;

                if ( csvRec.IsReportable ) CountReportable += csvRec.Count;
            }

            Logger.WriteToLog($"CSV input count total: {TotalCount}. CSV reportable count: {CountReportable}.");
            Console.WriteLine($"CSV input count total: {TotalCount}. CSV reportable count: {CountReportable}.");

            return CountReportable;
        }

        private bool CheckIsInt(string input)
        {
            int number = 0;
            return int.TryParse(input, out number);
        }
    }
}
